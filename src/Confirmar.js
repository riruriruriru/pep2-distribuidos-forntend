import React, { Component } from 'react';
import { ListGroup, Button, Image , Container, Form, Navbar, Row, Col, Media, InputGroup, Alert} from "react-bootstrap";
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import axios from 'axios';
import moment from 'moment';
import 'bootstrap/dist/css/bootstrap.min.css';
import { FcFile } from "react-icons/fc";
import localization from 'moment/locale/es';
import './Confirmar.css';

const qs = require('qs');

export default class Confirmar extends React.Component {
    constructor(props) {
    super(props);
    this.setShow = this.setShow.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    //this.formatearRut = this.formatearRut.bind(this);
      this.state = {
          permisoID: "",
          showInfoPermiso: false,
          permiso: "",
          permisoValido: false,
          permisoNoExiste: false,
          fechaInicio: "",
          horaInicio: "",
          fechaTermino: "",
          horaTermino: ""
          };

    }
    setShow(bool){
      this.setState({showInfoPermiso: !bool, fechaInicio: "",
          horaInicio: "",
          fechaTermino: "",
          horaTermino: "",
          permisoID: ""})
    }
    handleSubmit(){
      if(this.state.permisoID!==""){
        fetch("http://"+process.env.ipBack +":9000/api/permisos/getId"+"/"+this.state.permisoID)
           .then(response => {
            if (response.ok) {
                response.json();
            } 
            else {
                throw new Error('Something went wrong');
            }

            })
           .then(data => {
            if(data.permiso===undefined||data.permiso===null){
              this.setState({permiso: "", showInfoPermiso: false, permisoValido: false})
            }
            else{
              var fechaInicio = moment(data.permiso.fechaCreacion).locale('es').format('MMMM Do YYYY'); 
              var horaInicio =  moment(data.permiso.fechaCreacion).locale('es').format('H:mm:ss');
              var fechaTermino = moment(data.permiso.fechaPermisoLimite).locale('es').format('MMMM Do YYYY');
              var horaTermino = moment(data.permiso.fechaPermisoLimite).locale('es').format('H:mm:ss');
              if(data.bool===true){
                console.log("valido")
                this.setState({permiso: data.permiso, showInfoPermiso:true, permisoValido: true, fechaInicio: fechaInicio, horaInicio: horaInicio, fechaTermino: fechaTermino, horaTermino: horaTermino})
              }
              else{
                console.log("no valido")
                this.setState({permiso: data.permiso, showInfoPermiso:true, permisoValido: false, fechaInicio: fechaInicio, horaInicio: horaInicio, fechaTermino: fechaTermino, horaTermino: horaTermino})
              }
              
            }
            console.log(data.permiso)
            alert(data.message)
          })
           .catch(error=>
           {
              console.log('Hubo un problema con la petición Fetch en Postgresql:' + error.message);
              fetch("http://"+process.env.ipBack +":9000/api/permisos/getIdCassandra"+"/"+this.state.permisoID)
              .then(response=>response.json())
              .then(data=>{
                    if(data.permiso===undefined||data.permiso===null){
                      this.setState({permiso: "", showInfoPermiso: false, permisoValido: false})
                    }
                    else{
                      var fechaInicio = moment(data.permiso.fechaCreacion).locale('es').format('MMMM Do YYYY'); 
                      var horaInicio =  moment(data.permiso.fechaCreacion).locale('es').format('H:mm:ss');
                      var fechaTermino = moment(data.permiso.fechaPermisoLimite).locale('es').format('MMMM Do YYYY');
                      var horaTermino = moment(data.permiso.fechaPermisoLimite).locale('es').format('H:mm:ss');
                      if(data.bool===true){
                        console.log("valido")
                        this.setState({permiso: data.permiso, showInfoPermiso:true, permisoValido: true, fechaInicio: fechaInicio, horaInicio: horaInicio, fechaTermino: fechaTermino, horaTermino: horaTermino})
                      }
                      else{
                        console.log("no valido")
                        this.setState({permiso: data.permiso, showInfoPermiso:true, permisoValido: false, fechaInicio: fechaInicio, horaInicio: horaInicio, fechaTermino: fechaTermino, horaTermino: horaTermino})
                      }
                      
                    }
                    console.log(data.permiso)
                    alert(data.message)
                })
              
           })
      }
    }
    handleInputChange(event){
      const target = event.target;
      var value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;
      console.log(value)
      this.setState({
            [name]: value
            });
    }
    componentDidMount(){
      this.setState({showInfoPermiso: false, permisoValido: false, permisoID: "", permiso: "", permisoNoExiste: false})

    }

    render(){
      const showInfoPermiso = this.state.showInfoPermiso;
      const permisoValido= this.state.permisoValido;
      const permisoNoExiste=this.state.permisoNoExiste;
      return(
        <Container className="containerConsultar">
        {showInfoPermiso
        
        ?
          <Alert variant={permisoValido ?"success" :"danger"} onClose={() => this.setShow(this.state.showInfoPermiso)} dismissible>
                <Alert.Heading>ID Permiso: {this.state.permiso.id}</Alert.Heading>
                {permisoValido
                  ?<Alert.Heading>Permiso Válido</Alert.Heading>
                  :<Alert.Heading>Permiso no es Válido</Alert.Heading>
                }
                <p>Fecha Inicio: {this.state.fechaInicio}</p>
                  <p>Hora Inicio: {this.state.horaInicio}</p>
                  <p>Fecha Término: {this.state.fechaTermino}</p>
                  <p>Hora Término: {this.state.horaTermino}</p>
                  <p>Fecha Actual: {moment().format('MMMM Do YYYY')} Hora Actual: {moment().format('H:mm:ss')}</p>
                
              </Alert>
        :<p> </p>
        }
        <Form>
        <Form.Group>
          <Form.Label><b>Ingrese ID del permiso que desea consultar: </b></Form.Label>
          <Form.Control
            required
            type="text"
            value={this.state.permisoID}
            name="permisoID"
            onChange = {this.handleInputChange}
          />
        </Form.Group>
        <Button  className="boton" type="button"  onClick={(e) => this.handleSubmit(e)}>Consultar</Button>
      </Form>

      
      </Container>
        )

    }
}
