import React, { Component } from 'react';
import logo from './logos/logo28.png';
import './App.css';
import { Button, Image , Container, Form, Navbar, Row, Col, Media, InputGroup} from "react-bootstrap";
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import axios from 'axios';
import moment from 'moment';
import regiones from './urls.js'
import 'bootstrap/dist/css/bootstrap.min.css';
export default class Footer extends React.Component {
    constructor(props) {
    super(props);
        //this.handleLogin = this.handleLogin.bind(this);

    }
    
     render() {
      return (
        

         <Container fluid = "true" className="footer">
               <Row className="row1">
                  <Col className="col1">
                     <Row className="row2">
                     <a className="links" href="http://www.carabineros.cl/" >
                     Comisaría Virtual - Carabineros de Chile
                     </a>
                     </Row>
                     <Row className="row2">
                     <a className="links" href="http://digital.gob.cl/" >
                     Powered by División de Gobierno Digital
                     </a>
                     </Row>
                     <Row>
                     <a className="links" href="https://comisariavirtual.cl/politica-privacidad.html">Política de Privacidad</a>
                     </Row>
                  </Col>
                  <Col className="col2">
                     <p className= "links">
                        Si el sistema presenta problemas comuníquese con nosotros escribiendo al siguiente correo
                        comisaria.virtual@carabineros.cl
                     </p>
                  </Col>
               </Row>
              
               <Row className="bicolor">
                     <Col className="azul"></Col>
                       <Col className="rojo"> </Col>
               </Row>
            </Container>
         
          
      );
    }
}
