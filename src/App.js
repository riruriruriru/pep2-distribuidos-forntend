import React, { Component } from 'react';
import logo from './logos/logo28.png';
import './App.css';
import { ListGroup, Button, Image , Container, Form, Navbar, Row, Col, Media, InputGroup, Alert} from "react-bootstrap";
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import axios from 'axios';
import moment from 'moment';
import regiones from './urls.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './Header.js';
import Footer from './Footer.js';
import Alerta from './Alerta.js';
import {comprobarInput, formatearRut, validate} from './funciones.js'
import { FcFile } from "react-icons/fc";
import localization from 'moment/locale/es';
import Confirmar from './Confirmar.js';

const qs = require('qs');

export default class App extends React.Component {
    constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCheck = this.handleCheck.bind(this);
    //this.formatearRut = this.formatearRut.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleInputChangeRut = this.handleInputChangeRut.bind(this);
    this.setShow = this.setShow.bind(this);
    this.handleForm = this.handleForm.bind(this);
        //this.limpiarValores = this.limpiarValores.bind(this);
        //this.comprobarInput = this.comprobarInput.bind(this);
        //this.handleLogin = this.handleLogin.bind(this);
      this.state = {
          isLogged: false,
          firebaseUser: null,
          isLoading: true,
          user: null,
          validated:false,
          setValidated:false,
          regiones: [],
          comunas: [],
          rut: "",
          nombre: "",
          direccion: "",
          region: "",
          comuna: "",
          motivo1: false,
          motivo2: false,
          motivo3: false,
          motivo1check: false,
          motivo2check: false,
          motivo3check: false,
          jurada: false,
          regionSelected: false,
          showAlert: false,
          contentAlert: "",
          fechaInicio: "",
          horaInicio: "",
          fechaTermino: "",
          horaTermino: "",
          permisoID: "",
          showForm: true,

          };

    }
    handleCheck(event){
      const target = event.target;
      var value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;
      console.log(value)
      this.setState({
            [name]: value
            });
    }
    setShow(bool){
      this.setState({showAlert: !bool, fechaInicio: "",
          horaInicio: "",
          fechaTermino: "",
          horaTermino: "",
          permisoID: ""})
    }
    handleInputChange(event) {
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        if(name==="region" &&value!==""){
          var region = JSON.parse(value)
          console.log(region)
          console.log(region.nombre)
          fetch(regiones+"/"+region.codigo+"/comunas")
         .then(response => response.json())
         .then(data => 
          this.setState({comunas: data, isLoading: false, [name]:value, regionSelected:true})
          )         
          return;  
        }
        else if(name==="region"&&value===""){
          this.setState({[name]: value, regionSelected: false})
          return;
        }
        if(name==="jurada"){
          return this.setState({[name]: true})
        }
        this.setState({
            [name]: value
            });
        }
    handleInputChangeRut(event){
        const target = event.target;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        
        console.log("se cambio el valor de rut: "+value)
        if(comprobarInput(value)===true){
            value= formatearRut(value)
            this.setState({rut: value})
            return
        }
          else{
            return
        }

      }
    handleSubmit(event){
      moment.lang('es')
      moment.locale('es', localization);
      if(validate(this.state)){
        this.setState({validated: true})
      
      }
      else{
        this.setState({validated: true})
        console.log(this.state.jurada)
        alert("Rellene todos los campos")
        return;
      }
      
      var permiso = {nombre: "", direccion: "", comuna: "", region: "", rut: "", motivos: ""}
      permiso.nombre = this.state.nombre;
      permiso.rut = this.state.rut;
      permiso.comuna = JSON.parse(this.state.comuna).nombre;
      permiso.region = JSON.parse(this.state.region).nombre;
      permiso.direccion = this.state.direccion;
      permiso.motivos = "";
      if(this.state.motivo1===true){
        permiso.motivos= permiso.motivos+ "Compra de Alimentos/"  
      }
      if(this.state.motivo2===true){
        permiso.motivos= permiso.motivos+"Compra de Medicamentos/"  
      }
      if(this.state.motivo3===true){
        permiso.motivos=permiso.motivos+"Compra de Insumos Básicos"  
      }
     
      console.log("FORM SUBMITTED: ")
      console.log(permiso)
      axios.post("http://"+process.env.ipBack +":9000/api/permisos/add",qs.stringify(permiso))
      .then(response=>
        { console.log(response.data)
          var fechaInicio = moment(response.data.fechaCreacion).locale('es').format('MMMM Do YYYY'); 
          var horaInicio =  moment(response.data.fechaCreacion).locale('es').format('H:mm:ss');
          var fechaTermino = moment(response.data.fechaPermisoLimite).locale('es').format('MMMM Do YYYY');
          var horaTermino = moment(response.data.fechaPermisoLimite).locale('es').format('H:mm:ss');
          this.setState({showAlert: true, fechaInicio: fechaInicio, horaInicio: horaInicio, fechaTermino: fechaTermino, horaTermino: horaTermino, permisoID: response.data.id,
          validated:false,
          setValidated:false,
          rut: "",
          nombre: "",
          direccion: "",
          region: "",
          comuna: "",
          motivo1: false,
          motivo2: false,
          motivo3: false,
          jurada: false,
          regionSelected: false})
          alert("Permiso agregado satisfactoriamente \n"+
            " Fecha Inicio: "+fechaInicio+" Hora Inicio: "+horaInicio+"\n Fecha Termino: "+fechaTermino +" Hora Termino: "+ horaTermino+ " \nId Permiso: "+response.data.id)
        })
    };
    handleForm(event){
      console.log(event)
      if(event==="form"){
        if(this.state.showForm===true){
          return;
        }
        else{
          this.setState({showForm: true})
        }
      }
      else{
        if(this.state.showForm===true){
          this.setState({showForm: false})
        }
        else{
          return;
        }
      }
    }
    componentDidMount(){
      fetch(regiones)
         .then(response => response.json())
         .then(data => 
          this.setState({regiones: data, isLoading: false})
          )
         .then(console.log("dentro de component did mount, regiones: "+this.state.regiones))
      
        
     
    }
     render() {
      const validated = this.state.validated;
      const setValidated = this.state.setValidated;
      const isLoading = this.state.isLoading;
      const regiones = this.state.regiones;
      const regionSelected = this.state.regionSelected;
      const comunas = this.state.comunas;
      const showForm = this.state.showForm;
      if(isLoading===true){
        return(
           <Container className="app" fluid="true">
           <Header/>
           <Row>
           <Col className="col-xs-12 col-md-3">
           </Col>
           <Col className="col-xs-12 col-md-9">
           <Alerta/>

          <p>Cargando...</p>
          </Col>
          </Row>
          <Footer/>
          </Container>
          )
      }
      return (
        
        <Container className="app" fluid="true">
        <Header/>
        <Row>
        <Col className="col1">
        <ListGroup as="ul" className="left-col">
    <ListGroup.Item as="li" active type="submit" className="left-col2" onClick={()=>this.handleForm("form")}>
     <FcFile/>
     <b>Iniciar Trámite</b>
    </ListGroup.Item>
    <ListGroup.Item as ="li" active type="submit" className="left-col2" onClick={()=>this.handleForm("consultar")}>
    <b>Consultar Permiso</b>
    </ListGroup.Item>
    </ListGroup>

        </Col>
        <Col className="col-xs-12 col-md-9">
        {showForm
            ?<Container className= "body">
            <h1 className="permiso-compras">Permiso Temporal para Compras</h1>
            {this.state.showAlert
              ?<Alert variant="danger" onClose={() => this.setShow(this.state.showAlert)} dismissible>
                <Alert.Heading>ID Permiso: {this.state.permisoID}</Alert.Heading>
                <p>Fecha Inicio: {this.state.fechaInicio}</p>
                  <p>Hora Inicio: {this.state.horaInicio}</p>
                  <p>Fecha Término: {this.state.fechaTermino}</p>
                  <p>Hora Término: {this.state.horaTermino}</p>
                
              </Alert>
              :<Alerta/>
            }
        <Form noValidate validated={validated}>
      
        <Form.Group controlId="validationCustom02">
          <Form.Label>Nombres (Indique su nombre completo. Formato: Caaa Aaaa Paaa Caaa)</Form.Label>
          <Form.Control
            required
            type="text"
            value={this.state.nombre}
            name="nombre"
            onChange = {this.handleInputChange}
          />
          <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
        </Form.Group>
        <Form.Group controlId="validationCustom01">
          <Form.Label>RUN (Formato: sin puntos con guión. Ejemplo: 12345678-9)</Form.Label>
          <Form.Control
            name = "rut"
            required
            type="text"
            maxLength ="12" 
            value={this.state.rut} 
            onChange = {this.handleInputChangeRut}
          />
          <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
        </Form.Group>

        <Form.Group controlId="formBasicCheckbox">
        <Form.Label>Motivo</Form.Label>
        <Form.Check
          required
          type="checkbox"
          label="Compra de Alimentos"
          feedback="You must agree before submitting."
          name= "motivo1"
          checked = {this.state.motivo1}
          onChange = {this.handleCheck}

        />
        <Form.Check
          required
          type="checkbox"
          label="Compra de Medicamentos"
          feedback="You must agree before submitting."
          name="motivo2"
          checked = {this.state.motivo2}
          onChange = {this.handleCheck}
        />
        <Form.Check
          required
          type="checkbox"
          label="Compra de Insumos Básicos"
          feedback="You must agree before submitting."
          name="motivo3"
          checked= {this.state.motivo3}
          onChange = {this.handleCheck}
        />
        </Form.Group>
     
        <Form.Group  controlId="validationCustom04">
          <Form.Label>Región</Form.Label>
          <Form.Control value= {this.state.region} name="region" as="select" placeholder="Región" onChange={this.handleInputChange} required >
          <option key={1000} value={""}>...</option>
          {regiones.map((region) =>

                <option key={region.codigo} value={JSON.stringify(region)}>{region.nombre}</option>
                        )}
          </Form.Control>
          <Form.Control.Feedback type="invalid">
            Seleccione una región válida.
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group  controlId="validationCustom03">
          <Form.Label>Comuna</Form.Label>
          <Form.Control value={this.state.comuna} name="comuna" disabled={!regionSelected} as="select" placeholder="Comuna" onChange={this.handleInputChange} required >
          <option key={1000} value={""}>...</option>
          {comunas.map((comuna) =>

                <option key={comuna.codigo} value={JSON.stringify(comuna)}>{comuna.nombre}</option>
                        )}
          </Form.Control>
          <Form.Control.Feedback type="invalid">
            Seleccione una comuna válida.
          </Form.Control.Feedback>
        </Form.Group>
        
        <Form.Group  controlId="validationCustom05">
          <Form.Label>Dirección</Form.Label>
          <Form.Control type="text" placeholder="Dirección" required 
          type="text"
            value={this.state.direccion}
            name="direccion"
            onChange = {this.handleInputChange}
          />
          <Form.Control.Feedback type="invalid">
            Ingrese una dirección válida.
          </Form.Control.Feedback>
        </Form.Group>
      <Form.Group>
      <Form.Label>Declaración Jurada</Form.Label>
        <Form.Check type="radio"
        label="En caso de comprobarse falsedad en la declaración de la causal invocada, para requerir el presente documento, se incurrirá en las penas del Artículo 210 del Código Penal."
        feedback="Campo declaración jurada es obligatorio."
        value={this.state.jurada}
        required
        checked = {this.state.jurada}
        name="jurada"
        onChange = {this.handleInputChange}


         />
      </Form.Group>
      <Button  className="boton" type="button"  onClick={(e) => this.handleSubmit(e)}>Actualizar</Button>
    </Form>


            </Container>
          :<Confirmar/>
          }
            </Col>
            </Row>
           <Footer/>

          
        </Container>
      );
    }
}
