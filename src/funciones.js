const charRut = ["1","2","3","4","5","6","7","8","9","0", "-", ".", "K","k"]

export function comprobarInput(string){
      var largo = string.length
      var cont = 0
      console.log("///////////////")
      console.log("COMPROBAR INPUT: "+largo)
      console.log(string)
      for(var character of string){
        if(charRut.includes(character)===true){
          //console.log("CHAR: "+character)
          cont = cont+1
        }
        
      }
      if(cont===largo){
        return true
      }
      else{
        return false
      }
      
    } 
export function formatearRut(rut) {
      var actual = rut.replace(/^0+/, "");
      console.log(actual)
      if (actual !== '' && actual.length > 1) {
          var sinPuntos = actual.replace(/\./g, "");
          var actualLimpio = sinPuntos.replace(/-/g, "");
          var inicio = actualLimpio.substring(0, actualLimpio.length - 1);
          var rutPuntos = "";
          var i = 0;
          var j = 1;
          for (i = inicio.length - 1; i >= 0; i--) {
              var letra = inicio.charAt(i);
              rutPuntos = letra + rutPuntos;
              if (j % 3 === 0 && j <= inicio.length - 1) {
                  rutPuntos = "." + rutPuntos;
              }
              j++;
          }
          var dv = actualLimpio.substring(actualLimpio.length - 1);
          rutPuntos = rutPuntos + "-" + dv;
      }
      else{
        return rut;
      }
      console.log(actual)
      console.log(rutPuntos)
      return rutPuntos;
    }
export function validate(state){
  var motivos = false;
  if(state.motivo1!==false ||state.motivo2!==false||state.motivo3!==false){
    motivos = true;
    console.log("VALIDATE: "+motivos)
  }
  if(state.nombre===""||state.rut===""||motivos===false||state.jurada===false||state.region===""||state.comuna===""||state.direccion===""){
    return false;
  }
  else{

    return true;
  }
}